<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
	public function Home(){
		$tasks = [
			'go to store',
			'go to the market',
			'go to work'
		];

		return view('welcome', [
			'tasks' => $tasks,
			'foo' => 'jobs'
		]);
	}

	public function About(){
		return view('about')->with([
			'locations' => [
				'Baghdad',
				'Erbil',
				'Basra'
			]
		]);
	}

	public function Contact(){
		return view('contact')->withName('Abdu');
	}

}
