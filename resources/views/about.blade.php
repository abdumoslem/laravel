@extends('layout')

@section('title', 'About us')

@section('body')
    <h1>Our offices:</h1>

    <ul>
        @foreach($locations as $location)
            <li>{{ $location }}</li>
        @endforeach
    </ul>
@endsection