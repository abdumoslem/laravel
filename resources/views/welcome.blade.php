@extends('layout')

@section('body')
    <h1>my {{ $foo }} website</h1>

    <ul>
        @foreach($tasks as $task)
            <li>{{ $task }}</li>
        @endforeach
    </ul>
@endsection